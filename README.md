# p2211932 - Yara Nefaa 

# TP3 Tiw8 
This is a clone of gather town.

## How to install locally

`yarn install` (install dependencies)
`yarn build` (build)
`yarn start` (start server)
`yarn dev` (start in dev mode for testing)

## How to test

The website is deployed on :https://gathertownclone.onrender.com/ . You can test it online or locally (to test it locally, use the instructions above to install and build and then proceed).
To test it, choose an avatar and then move it (with up, right, left, down arrows). Open another tab or window with the same link, choose an avatar, and move it close to other avatars. When the distance is less than 1, a video call will start. If you move further the volume will go down. When you go further than 5 steps, the video call is closed. You can also hover over avatar ids to see their information.

## Features

Tailwind styles +
State and props used +
Hooks properly used +
Peers connected and server signaling +
Peers can send messages to eachother +
A middleware handles messages transfer +
Members of the board move with coherence +
The local flow appears +
The remote flow appears +
All functions on localhost +
The call is ended when close the tab +
Deployment works +
Managing more than 2 peers +
Managing audio and video separately