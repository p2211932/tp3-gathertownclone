import * as React from 'react'
import { Board } from './components/Board'

export const App: React.FC = () => {
    return (
        <div className="flex flex-col bg-cyan-600">
            <Board />
        </div>
    )
}
