import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { mapValues, omit } from 'lodash'
import { Instance } from 'simple-peer'
import {
    movePlayerAction,
    applyAvatarAction,
    remotePlayer,
    updateRemoteVideoStreamAction,
} from '../types'

interface AppState {
    playerPosition: [number, number]
    playerAvatar: string
    remotePositions: { [key: string]: [number, number] }
    remoteAvatars: { [key: string]: string }
    distances: { [key: string]: number }
    board: {
        width: number
        height: number
    }
    stream: MediaStream
    remote: { [key: string]: remotePlayer }
    peers: { [key: string]: Instance }
    connectedTo: { [key: string]: boolean }
}

const initialState: AppState = {
    playerPosition: [
        Math.round(Math.random() * 10),
        Math.round(Math.random() * 10),
    ],
    playerAvatar: ['Yara', 'Paul', 'Charbel', 'John'][
        Math.floor(Math.random() * 4)
    ],
    remotePositions: {},
    remoteAvatars: {},
    distances: {},
    board: {
        width: 60,
        height: 60,
    },
    stream: undefined,
    remote: {},
    peers: {},
    connectedTo: {},
}

export const boardPart = createSlice({
    name: 'board',
    initialState,
    reducers: {
        movePlayer: {
            reducer(state, action: PayloadAction<movePlayerAction>) {
                const { position, peerId } = action.payload
                if (!peerId) {
                    state.playerPosition = position
                } else {
                    state.remotePositions[peerId] = position
                }
                return state
            },
            prepare(payload: movePlayerAction, propagate: boolean) {
                return {
                    payload: payload,
                    meta: { propagate },
                }
            },
        },
        applyAvatar: {
            reducer(state, action: PayloadAction<applyAvatarAction>) {
                const { avatar, peerId } = action.payload

                if (!peerId) {
                    state.playerAvatar = avatar
                } else {
                    state.remoteAvatars[peerId] = avatar
                }
                return state
            },
            prepare(payload: applyAvatarAction, propagate: boolean) {
                return { payload, meta: { propagate } }
            },
        },
        setStream: {
            reducer: (state, action: PayloadAction<MediaStream>) => {
                state.stream = action.payload
                return state
            },
            prepare: (payload: MediaStream, propagate: boolean) => {
                return { payload, meta: { propagate } }
            },
        },
        updateRemoteVideoStream: (
            state,
            action: PayloadAction<updateRemoteVideoStreamAction>
        ) => {
            const { stream, peerId } = action.payload
            if (stream) {
                state.remote[peerId].stream = stream
            } else {
                const remoteStream = state.remote[peerId].stream
                if (remoteStream) {
                    state.remote[peerId].stream = undefined
                }
            }
        },
        breakStream: {
            reducer: (state) => {
                state.stream = undefined
            },
            prepare: (payload: MediaStream, propagate: boolean) => {
                return { payload, meta: { propagate } }
            },
        },
        calculDistance: (state) => {
            mapValues(state.remotePositions, (position, peerId) => {
                state.distances[peerId] =
                    Math.abs(state.playerPosition[0] - position[0]) +
                    Math.abs(state.playerPosition[1] - position[1])
            })
        },
        deletePeer: {
            reducer: (state, action: PayloadAction<string>) => {
                const peerId = action.payload
                state.peers[peerId].destroy()
                state.peers = omit(state.peers, peerId)
                state.remote = omit(state.remote, peerId)
                state.remoteAvatars = omit(state.remoteAvatars, peerId)
                state.remotePositions = omit(state.remotePositions, peerId)
                state.distances = omit(state.distances, peerId)
            },
            prepare: (payload: string, propagate: boolean) => {
                return { payload, meta: { propagate } }
            },
        },
        addPeer: (
            state,
            action: PayloadAction<{ peerId: string; peer?: Instance }>
        ) => {
            const { peerId, peer } = action.payload

            state.remote[peerId] = {}
            state.peers[peerId] = peer
        },
        sendStream: (state, action: PayloadAction<string>) => {
            const stream = state.stream
            const peerId = action.payload
            const peers = mapValues(state.peers, (peer) => peer)
            const target = peers[peerId]
            const cnd = state.remote[peerId].currentlyStreamingTo
            !cnd && target && stream && target.addStream(stream)
            state.remote[peerId].currentlyStreamingTo = true
            state.connectedTo[peerId] = true
        },
        removeStream: (state, action: PayloadAction<string>) => {
            const peerId = action.payload
            state.connectedTo[peerId] = false
        },
    },
})

export const {
    movePlayer,
    applyAvatar,
    setStream,
    updateRemoteVideoStream,
    breakStream,
    calculDistance,
    addPeer,
    sendStream,
    removeStream,
    deletePeer,
} = boardPart.actions

export default boardPart.reducer
