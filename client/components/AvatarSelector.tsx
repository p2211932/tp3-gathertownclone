import * as React from 'react'
import { useDispatch } from 'react-redux'
import { AppDispatch } from '../store'
import { useAppSelector } from '../hooks'
import { applyAvatar } from '../boardparts/boardPart'
import John from '../img/John.png'
import Paul from '../img/Paul.png'
import Charbel from '../img/Charbel.png'
import Yara from '../img/Yara.png'
import { useCallback } from 'react'

export const AvatarSelector: React.FC = () => {
    const playerAvatar: string = useAppSelector((state) => state.playerAvatar)

    const dispatch = useDispatch<AppDispatch>()

    const avatarSelected = useCallback(
        (event) => {
            const avatar = event.currentTarget.id
            dispatch(applyAvatar({ avatar }, true))
        },
        [playerAvatar]
    )

    return (
        <div className="flex flex-col order-2" id="avatarList">
            <button
                id="Charbel"
                onClick={avatarSelected}
                className={playerAvatar === 'Charbel' ? 'selected' : ''}
            >
                <img className="m-2" src={Charbel}></img>
            </button>
            <button
                id="John"
                onClick={avatarSelected}
                className={playerAvatar === 'John' ? 'selected' : ''}
            >
                <img className="m-2" src={John}></img>
            </button>
            <button
                id="Yara"
                onClick={avatarSelected}
                className={playerAvatar === 'Yara' ? 'selected' : ''}
            >
                <img className="m-2" src={Yara}></img>
            </button>
            <button
                id="Paul"
                onClick={avatarSelected}
                className={playerAvatar === 'Paul' ? 'selected' : ''}
            >
                <img className="m-2" src={Paul}></img>
            </button>
        </div>
    )
}
