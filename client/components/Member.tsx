import {
    Card,
    CardBody,
    TableContainer,
    Table,
    TableBody,
    TableRow,
    TableCell,
} from '@windmill/react-ui'
import React, { FC, useState } from 'react'
import { useAppSelector } from '../hooks'
import John from '../img/John.png'
import Paul from '../img/Paul.png'
import Charbel from '../img/Charbel.png'
import Yara from '../img/Yara.png'

interface props {
    peerId: string
    idx: number
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const avatars: { [key: string]: any } = {
    John: John,
    Yara: Yara,
    Paul: Paul,
    Charbel: Charbel,
}
const Member: FC<props> = ({ peerId, idx }) => {
    const [hide, setHide] = useState(true)
    const remoteAvatars = useAppSelector((state) => state.remoteAvatars)
    const remotePositions = useAppSelector((state) => state.remotePositions)
    const distances = useAppSelector((state) => state.distances)
    return (
        <>
            <button
                className="border-red-400 border-2 h-12 w-12 p-2 flex justify-center items-center rounded-md shadow-lg bg-red-700 hover:bg-cyan-500 mb-2"
                onMouseEnter={() => setHide(false)}
                onMouseLeave={() => setHide(true)}
            >
                {idx}
            </button>
            {!hide && (
                <div className="absolute m-3">
                    <Card>
                        <CardBody className="bg-cyan-500">
                            <TableContainer>
                                <Table>
                                    <TableBody>
                                        {remoteAvatars[peerId] && (
                                            <TableRow>
                                                <TableCell>Avatar</TableCell>
                                                <TableCell>
                                                    <img
                                                        src={
                                                            avatars[
                                                                remoteAvatars[
                                                                    peerId
                                                                ]
                                                            ]
                                                        }
                                                    />
                                                </TableCell>
                                            </TableRow>
                                        )}
                                        <TableRow>
                                            <TableCell>ID of peer</TableCell>
                                            <TableCell>{peerId}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Position</TableCell>
                                            <TableCell>
                                                {remotePositions[peerId][0]},{' '}
                                                {remotePositions[peerId][0]}
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Distance</TableCell>
                                            <TableCell>
                                                {distances[peerId]}
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </CardBody>
                    </Card>
                </div>
            )}
        </>
    )
}

export default Member
