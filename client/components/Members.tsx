import { toPairs } from 'lodash'
import React, { FC } from 'react'
import { useAppSelector } from '../hooks'
import Member from './Member'

const Members: FC = () => {
    const remote = useAppSelector((state) => state.remote)

    const remotePlayersDisplay = toPairs(remote).map(([peerId], idx) => {
        return (
            <div key={peerId}>
                <Member peerId={peerId} idx={idx} />
            </div>
        )
    })

    return (
        <div className="top-0 m-2 flex flex-col text-gray-100 ">
            <span className="text-lg font-semibold">In town</span>
            <div className="border-red-400 border-2 h-12 w-12 p-2 flex justify-center items-center rounded-md shadow-lg bg-red-700 hover:bg-cyan-500 m-2">
                <span className="text-sm">none</span>
            </div>
            {remotePlayersDisplay}
        </div>
    )
}

export default Members
