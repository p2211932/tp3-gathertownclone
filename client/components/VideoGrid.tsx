import { mapValues } from 'lodash'
import React, { FC, useEffect, useRef } from 'react'
import { useAppSelector } from '../hooks'
import VideoWindow from './VideoWindow'

const VideoGrid: FC = () => {
    const localVideoRef = useRef<HTMLVideoElement>()

    const localStream = useAppSelector((state) => state.stream)
    const connectedTo = useAppSelector((state) => state.connectedTo)
    const distances = useAppSelector((state) => state.distances)
    const remoteStreams = useAppSelector((state) =>
        mapValues(state.remote, ({ stream }) => stream)
    )
    const gotStream = (stream: MediaProvider) => {
        if (localVideoRef.current) {
            localVideoRef.current.srcObject = stream
        }
        return stream
    }

    useEffect(() => {
        gotStream(localStream)
    }, [localStream])

    return (
        <div className="item flex flex-col w-full order-4 p-4 space-y-4">
            <div className="video-container h-1/3 w-full flex justify-center items-start">
                <video
                    className="w-full h-auto object-cover"
                    ref={localVideoRef}
                    autoPlay
                    muted
                >
                    <track
                        kind="captions"
                        srcLang="en"
                        label="english_captions"
                    />
                </video>
            </div>
            <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-4 auto-rows-min overflow-y-auto">
                {Object.entries(remoteStreams).map(
                    ([peerId, stream]) =>
                        connectedTo[peerId] && (
                            <VideoWindow
                                peerId={peerId}
                                mediaSource={stream}
                                key={`VideoWindow-${peerId}`}
                                proximity={distances[peerId]}
                            />
                        )
                )}
            </div>
        </div>
    )
}
export default VideoGrid
