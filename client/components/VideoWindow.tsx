import React, { FunctionComponent, useEffect, useRef } from 'react'

interface VideoProps {
    mediaSource: MediaStream
    peerId: string
    proximity: number
}

const RemoteVideo: FunctionComponent<VideoProps> = ({
    mediaSource,
    peerId,
    proximity,
}) => {
    const videoElementRef = useRef<HTMLVideoElement>(null)

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const attachMediaStream = (stream: MediaStream, _peerId: string) => {
        if (videoElementRef.current) videoElementRef.current.srcObject = stream
    }

    useEffect(() => {
        attachMediaStream(mediaSource, peerId)
    }, [mediaSource, peerId])

    useEffect(() => {
        if (videoElementRef.current)
            videoElementRef.current.volume = proximity === 0 ? 1 : 1 / proximity
    }, [proximity])

    return (
        <div className="flex justify-center items-center h-full">
            <video
                className="border-red-700 border-4 rounded-md object-cover h-auto w-full max-h-[90%]"
                ref={videoElementRef}
                autoPlay
                playsInline
            >
                <track kind="captions" srcLang="en" label="English Captions" />
            </video>
        </div>
    )
}

export default RemoteVideo
