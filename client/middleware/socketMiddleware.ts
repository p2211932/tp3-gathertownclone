import { AnyAction, Dispatch, Middleware } from 'redux'
import * as io from 'socket.io-client'
import Peer from 'simple-peer'
import { toPairs } from 'lodash'
import { applicationStore } from '../store'
import {
    movePlayer,
    applyAvatar,
    updateRemoteVideoStream,
    addPeer as ap,
    deletePeer as dp,
} from '../boardparts/boardPart'
import { movePlayerAction, applyAvatarAction, restructuredData } from '../types'

const socket = io.connect()
const useTrickle = true

const addPeer = (socketId: string, initiator: boolean) => {
    const peer = new Peer({
        initiator,
        trickle: useTrickle, // useTrickle doit être a true pour que le peer persiste
        config: {
            iceServers: [
                { urls: 'stun:stun.l.google.com:19302' },
                { urls: 'stun:global.stun.twilio.com:3478' },
            ],
        },
    })

    peer.on('signal', (signal) => {
        socket.emit('signal', {
            signal,
            socketId,
        })
    })
    peer.on('connect', () => {
        console.log(`Peer connection established`)
        const avatar = applicationStore.getState().playerAvatar
        const position = applicationStore.getState().playerPosition
        avatar && applicationStore.dispatch(applyAvatar({ avatar }, true))
        applicationStore.dispatch(movePlayer({ position }, true))
    })
    peer.on('error', (err) =>
        console.log(`Error sending data to peer : ${err}`)
    )
    peer.on('data', (data) => {
        const restructuredData: restructuredData = JSON.parse(data)
        const { type, payload } = restructuredData

        switch (type) {
            case 'movePlayer':
                applicationStore.dispatch(
                    movePlayer(
                        {
                            position: (<movePlayerAction>payload).position,
                            peerId: socketId,
                        },
                        false
                    )
                )
                break
            case 'applyAvatar':
                applicationStore.dispatch(
                    applyAvatar(
                        {
                            avatar: (<applyAvatarAction>payload).avatar,
                            peerId: socketId,
                        },
                        false
                    )
                )
                break
        }
    })
    peer.on('stream', (stream) => {
        stream.addEventListener('inactive', () =>
            applicationStore.dispatch(
                updateRemoteVideoStream({ stream: undefined, peerId: socketId })
            )
        )
        applicationStore.dispatch(
            updateRemoteVideoStream({ stream, peerId: socketId })
        )
    })

    applicationStore.dispatch(ap({ peerId: socketId, peer }))
}

const deletePeer = (socketId: string) => {
    applicationStore.dispatch(dp(socketId, true))
}

socket.on('initReceive', (socketId) => {
    addPeer(socketId, false)
    socket.emit('initSend', socketId)
})

socket.on('initSend', (socketId) => {
    addPeer(socketId, true)
})

socket.on('signal', (data: { socketId: string; signal: Peer.SignalData }) => {
    const connectedPeers = applicationStore.getState().peers
    const { socketId, signal } = data
    connectedPeers[socketId].signal(signal)
})

socket.on('deletePeer', (socketId) => {
    deletePeer(socketId)
    console.log(`deleting peer ... peer id : ${socketId}`)
})

socket.on('disconnect', () => {
    const connectedPeers = applicationStore.getState().remote
    console.log(`disconnecting...`)
    Object.entries(connectedPeers).forEach(([peerId]) => {
        deletePeer(peerId)
    })
})

export const actionMiddleware: Middleware<Dispatch> =
    () => (next) => (action: AnyAction) => {
        const { meta, type, payload } = action
        const [partName, reducer] = type.split('/')
        const peers = applicationStore.getState().peers

        if (meta?.propagate) {
            if (partName === 'board') {
                const message = JSON.stringify({
                    type: reducer,
                    payload,
                })

                switch (reducer) {
                    case 'movePlayer':
                        // eslint-disable-next-line @typescript-eslint/no-unused-vars
                        toPairs(peers).forEach(([_, peer]) => {
                            peer.send(message)
                        })
                        break
                    case 'applyAvatar':
                        // eslint-disable-next-line @typescript-eslint/no-unused-vars
                        toPairs(peers).forEach(([_, peer]) =>
                            peer.send(message)
                        )
                        break
                }
            }
        }

        return next(action)
    }
