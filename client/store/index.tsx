import { configureStore } from '@reduxjs/toolkit'
import boardReducer from '../boardparts/boardPart'
import { actionMiddleware } from '../middleware/socketMiddleware'

export const applicationStore = configureStore({
    reducer: boardReducer,
    middleware: [actionMiddleware],
})

export type RootState = ReturnType<typeof applicationStore.getState>
export type AppDispatch = typeof applicationStore.dispatch
