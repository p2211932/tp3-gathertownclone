import SimplePeer from 'simple-peer'

type RouteParams = {
    id: string // parameters will always be a string (even if they are numerical)
}

interface Member {
    initiator: boolean
    peerId: string
}

interface movePlayerAction {
    position: [number, number]
    peerId?: string
}

interface applyAvatarAction {
    avatar: string
    peerId?: string
}

interface updateRemoteVideoStreamAction {
    stream: MediaStream
    peerId: string
}

interface remotePlayer {
    stream?: MediaStream
    currentlyStreamingTo?: boolean
}

interface genAction<T, P> {
    type: T
    payload: P
}
type restructuredData =
    | genAction<'movePlayer', movePlayerAction>
    | genAction<'applyAvatar', applyAvatarAction>
    | genAction<'removeStream', string>
